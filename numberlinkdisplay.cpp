#include "numberlinkdisplay.h"
#include <QDebug>
#include <QDateTime>

NumberlinkDisplay::NumberlinkDisplay()
{
    isInitialized = false;
    mGenerator = NULL;
    isSolutionFound = false;
    inputFileName = "";
}

void NumberlinkDisplay::initUI()
{
    if(mGenerator == NULL || isInitialized)
        return;

    mGenerator->isNoBlankCell = false;

    mainWindow = new MainWindow();
    mainWidget = new QWidget();
    mainWidgetLayout = new QVBoxLayout(mainWidget);

    chooseInputBtn = new QPushButton();
    chooseInputBtn->setText("Choose input file");
    mainWidgetLayout->addWidget(chooseInputBtn);

    dialog.setParent(NULL);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setViewMode(QFileDialog::Detail);

    nextSolutionBtn = new QPushButton();
    nextSolutionBtn->setText("Find Solution");
    mainWidgetLayout->addWidget(nextSolutionBtn);

    noBlankCellCbk = new QCheckBox("No blank cell");
    noBlankCellCbk->setChecked(false);
    mainWidgetLayout->addWidget(noBlankCellCbk);

    runningTimeLabel = new QLabel();
    runningTimeLabel->setText("");
    mainWidgetLayout->addWidget(runningTimeLabel);

    mainLayout = new QGridLayout();
    mainLayout->setSpacing(2);

    mainWidgetLayout->addLayout(mainLayout);
    mainWidget->setLayout(mainWidgetLayout);
    mainWindow->setCentralWidget(mainWidget);

    connect(nextSolutionBtn, SIGNAL(clicked()), this, SLOT(findNextSolution()));
    connect(noBlankCellCbk, SIGNAL(stateChanged(int)),this, SLOT(onNoBlankCellChange(int)));
    connect(chooseInputBtn, SIGNAL(clicked()), this, SLOT(onChooseInputFile()));
    isInitialized = true;
}

void NumberlinkDisplay::resetUI()
{
    foreach(QPushButton* oldButton, itemManagers){
        mainLayout->removeWidget(oldButton);
        delete oldButton;
        oldButton = NULL;
    }
    itemManagers.clear();

    if(!mainWindow || !mainLayout)
        return;
    nextSolutionBtn->setEnabled(true);
    nextSolutionBtn->setText("Find solution");

    for(int i = 1; i <= mGenerator->m_rowcount; i++){
        for(int j = 1; j <= mGenerator->m_columncount; j++){
            QPushButton* newButton = new QPushButton(mainWidget);
            itemManagers.append(newButton);
            newButton->setFixedHeight(40);
            newButton->setFixedWidth(40);
            newButton->setStyleSheet("background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)");
            if(mGenerator->isNumber(mGenerator->inputs[i][j])){
                newButton->setText(mGenerator->inputs[i][j]);
            }
            else{
                newButton->setText("");
            }
            mainLayout->addWidget(newButton,i-1,j-1);
        }
    }
    mainWindow->repaint();
}

void NumberlinkDisplay::displayUI()
{
    initUI();
    mainWindow->show();
}

Generator *NumberlinkDisplay::getGenerator() const
{
    return mGenerator;
}

void NumberlinkDisplay::setGenerator(Generator *value)
{
    mGenerator = value;
    connect(mGenerator, SIGNAL(sigFindSolutionFinished(bool)), this, SLOT(onFindSolutionFinished(bool)));
}

void NumberlinkDisplay::onNoBlankCellChange(int state)
{
    if(state == Qt::Checked)
        mGenerator->isNoBlankCell = true;
    else
        mGenerator->isNoBlankCell = false;
}
void NumberlinkDisplay::findNextSolution()
{
    if(mGenerator != NULL && isInitialized){
        //prevent calling process multiple times
        nextSolutionBtn->setEnabled(false);
        if(!isSolutionFound){
            qDebug() << "findFirstSolution";
            mGenerator->generateCnfFile(inputFileName,curWorkingDir.path() + "/" + INPUT_CNF_NAME);
        }
        else{
            qDebug() << "findNextSolution";
            mGenerator->tryNext(curWorkingDir.path() + "/" + INPUT_CNF_NAME,curWorkingDir.path() + "/" + RESULT_FILE_NAME);
        }
        mStartTIme = QDateTime::currentDateTime();
        qDebug() << "Start finding solution at: " << mStartTIme;
        mGenerator->findSolution(inputFileName,curWorkingDir.path() + "/" + INPUT_CNF_NAME,curWorkingDir.path() + "/" +RESULT_FILE_NAME);
    }
}

void NumberlinkDisplay::onFindSolutionFinished(bool resultStatus)
{
    nextSolutionBtn->setEnabled(true);

    if(resultStatus){
        isSolutionFound = true;
        noBlankCellCbk->setVisible(false);
    }
    else{
        isSolutionFound = false;
        nextSolutionBtn->setText( isSolutionFound? "No more solution found!" : "No solution found!");
        nextSolutionBtn->setEnabled(false);
    }
    if(!isSolutionFound)
        return;
    int linearIndex;
    for(int i = 1; i <= mGenerator->m_rowcount; i++){
        for(int j = 1; j <= mGenerator->m_columncount; j++){
            linearIndex = (i-1) * mGenerator->m_columncount + j ;
            QPushButton* newButton = itemManagers.at(linearIndex-1);
            newButton->setFixedHeight(40);
            newButton->setFixedWidth(40);
            if(mGenerator->outputs[i][j].isNumber){
                newButton->setStyleSheet("background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)");
                newButton->setText(mGenerator->inputs[i][j]);
            }
            else{
                QIcon newIcon(mGenerator->outputs[i][j].imagePath);
                newButton->setIconSize(QSize(40,40));
                newButton->setIcon(newIcon);
            }
        }
    }
    QDateTime currentTime = QDateTime::currentDateTime();
    runningTimeLabel->setText("Running time: " + QString::number(mStartTIme.msecsTo(currentTime)) + " ms");
    mainWindow->repaint();
    qDebug() << "Found solution at: " << currentTime;
}

void NumberlinkDisplay::onChooseInputFile()
{
    runningTimeLabel->setText("");
    QStringList fileNames;
    if (dialog.exec())
        fileNames = dialog.selectedFiles();
    if(fileNames.size() > 0){
        inputFileName = fileNames.at(0);
        qDebug() << inputFileName;
        if(inputFileName != ""){
            mGenerator->parseInput(inputFileName);
            curWorkingDir = QFileInfo(inputFileName).absoluteDir();
            isSolutionFound = false;
        }

        resetUI();
        mainWindow->repaint();
    }
}
