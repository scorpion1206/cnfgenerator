#ifndef NUMBERLINKDISPLAY_H
#define NUMBERLINKDISPLAY_H

#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <generator.h>
#include <QDateTime>
#include <QLabel>
#include "mainwindow.h"

#define INPUT_FILE_NAME "input.txt"
#define INPUT_CNF_NAME "input.cnf"
#define RESULT_FILE_NAME "output.txt"

class NumberlinkDisplay : public QObject
{
    Q_OBJECT
public:
    explicit NumberlinkDisplay();

    void initUI();
    void resetUI();
    void displayUI();
    Generator *getGenerator() const;
    void setGenerator(Generator *value);

signals:

public slots:
    void onNoBlankCellChange(int state);
    void findNextSolution();
    void onFindSolutionFinished(bool resultStatus);
    void onChooseInputFile();

private:
    Generator* mGenerator;
    bool isInitialized;
    bool isSolutionFound;
    MainWindow* mainWindow;
    QWidget* mainWidget;
    QVBoxLayout* mainWidgetLayout;
    QPushButton* nextSolutionBtn;
    QPushButton* chooseInputBtn;
    QCheckBox* noBlankCellCbk;
    QGridLayout* mainLayout;
    QLabel* runningTimeLabel;
    QList<QPushButton*> itemManagers;
    QString inputFileName;
    QDir curWorkingDir;
    QFileDialog dialog;
    QDateTime mStartTIme;
};

#endif // NUMBERLINKDISPLAY_H
