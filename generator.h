#ifndef GENERATOR_H
#define GENERATOR_H

#include <QObject>
#include <QFileDialog>
#include <QGridLayout>
#include <QProcess>
#define NUM_OF_DIRECTION 4
#define MAX_INPUT_SIZE 100

enum DIRECTIONS{
   LEFT =  1,
   RIGHT = 2,
   UP = 3,
   DOWN = 4
};

enum STATUS{
    VALID_INPUT = 0,
    INVALID_INPUT = 1,
    NOT_ENOUGH_PAIR = 2
};

struct SolutionItem{
    bool isLeft;
    bool isRight;
    bool isUp;
    bool isDown;
    bool isNumber;
    QString imagePath;
};

const int DIR[][2] = { {-1000,-1000},  {0,-1}, {0,1}, {-1,0}, {1,0} };

#define MAX_PERMUTATION_SIZE 100
#include <QSet>
#include <QStringList>
class Generator : public QObject
{
    Q_OBJECT
public:
    explicit Generator(QObject *parent = 0);

    bool parseResults(QString inputFileName, QString resultFileName);
    bool isResultValid(QString cnfFileName, QString resultFileName, QStringList &newConstrain);
    void removeResultPosition(int i, int j, SolutionItem results[MAX_INPUT_SIZE][MAX_INPUT_SIZE]);
    bool tryNext(QString cnfFileName, QString resultFileName);
    void addConstrain2Input(QString cnfFileName, QStringList constrains);
    STATUS parseInput(QString inputFileName);
    void generateCnfFile(QString inputFileName, QString outputFileName);
    bool isNumber(QString inputString);

    int toPosition(int i,int j, int value);

    QStringList connectToNumCell(int i, int j, int num);
    QStringList exact_one_direction(int i, int j);
    QStringList atleast_one_direction(int i, int j);

    QStringList maybeBlankCell(int i, int j);
    QStringList maybe_two_direction(int i, int j);
    QStringList connect_same_number(int i, int j);    

    QStringList maybe_one_number(int i, int j);
    QStringList limit_boundary(int i, int j);
    void generatePermutation(int num, int base, int step, int prev);

    int maxNum() const;
    void setMaxNum(int maxNum);
    void initPermutation();

    QList< QSet<int> > getPermutationResult() const;
    void setPermutationResult(const QList<QSet<int> > &value);

    int m_maxNum;
    int m_rowcount;
    int m_columncount;
    int m_status;
    bool isNoBlankCell;
    QString m_prevInputFileName;
    QString m_prevInputCnfName;
    QString m_prevResultFileName;
    QString inputs[MAX_INPUT_SIZE][MAX_INPUT_SIZE];
    SolutionItem outputs[MAX_INPUT_SIZE][MAX_INPUT_SIZE];
    bool findSolution(QString inputFileName, QString cnfFileName, QString outputFileName);
    void runMinisat(QString inputCnf, QString output);
    QProcess *m_process;
signals:
    void sigFindSolutionFinished(bool resultStatus);
public slots:
    void onMinisatFinished(int exit_status);
private:
    bool m_mark[MAX_PERMUTATION_SIZE];
    int m_limit[5];
    QList< QSet<int> > permutationResult;
};

#endif // GENERATOR_H
