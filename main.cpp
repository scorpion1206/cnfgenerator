#include "mainwindow.h"
#include <QApplication>
#include "generator.h"
#include <QDebug>
#include <QProcess>
#include <numberlinkdisplay.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Generator mGenerator;

    NumberlinkDisplay mNumbelinkDisplay;
    mNumbelinkDisplay.setGenerator(&mGenerator);
    mNumbelinkDisplay.displayUI();

    return a.exec();
}
