#include "generator.h"
#include <QDebug>
#include <QFile>
#include <QtAlgorithms>
#include <QDateTime>
#include <qmath.h>
#include <QApplication>
#include <QProcess>
#include <mainwindow.h>

Generator::Generator(QObject *parent) :
    QObject(parent)
{
    m_columncount = 10;
    m_rowcount = 10;
    m_maxNum = 8;
    m_limit[LEFT] = 1;
    m_limit[UP] = 1;
    isNoBlankCell = false;
    m_limit[RIGHT] = m_columncount;
    m_limit[DOWN] = m_rowcount;
    m_process  = new QProcess(qApp);
}

bool Generator::parseResults(QString inputFileName, QString resultFileName)
{
    if(parseInput(inputFileName) != VALID_INPUT)
        return false;
    QFile resultFile(resultFileName);
    if(!resultFile.open(QFile::ReadOnly))
        return false;

    m_prevInputFileName = inputFileName;
    m_prevResultFileName = resultFileName;

    QTextStream resultTextStream(&resultFile);
    QString resultInput;
    if((resultInput =resultTextStream.readLine()) == "UNSAT")
        return false;
    resultInput = resultTextStream.readLine();
    QStringList resultList = resultInput.trimmed().split(" ");
    int singleItemCount = NUM_OF_DIRECTION + m_maxNum;
    int indexRow;
    int indexCol;
    int indexValue;

    bool isLeft, isRight, isUp,isDown;

    int listSize = resultList.size();
    for(int i = 1; i <= listSize - singleItemCount + 1; i += singleItemCount){

        isLeft = isRight = isUp = isDown = false;
        indexValue = ( i % singleItemCount == 0) ? singleItemCount : (i % singleItemCount);

        int tmp = (i - indexValue) / singleItemCount;
        indexCol = (tmp % m_columncount == 0) ? 1 : tmp % m_columncount + 1;
        tmp = (indexCol - 1) * singleItemCount + indexValue;
        indexRow = (i - tmp)/(singleItemCount * m_columncount) + 1;

        bool isItemNumber = false;
        if(isNumber(inputs[indexRow][indexCol])){
            isItemNumber = true;
        }
        outputs[indexRow][indexCol].isNumber = isItemNumber;

        if(resultList.at(i-1).toInt() > 0){
            isLeft = true;
        }
        if(resultList.at(i).toInt() > 0){
            isRight = true;
        }
        if(resultList.at(i + 1).toInt() > 0){
            isUp = true;
        }
        if(resultList.at(i + 2).toInt() > 0){
            isDown = true;
        }
        outputs[indexRow][indexCol].isLeft = isLeft;
        outputs[indexRow][indexCol].isRight = isRight;
        outputs[indexRow][indexCol].isUp = isUp;
        outputs[indexRow][indexCol].isDown = isDown;

        if(!isItemNumber){
            QString itemPath = ":/icons/icons/Blank.png";
            if(isLeft && isDown)
                itemPath = ":/icons/icons/DownLeft.png";
            if(isLeft && isRight)
                itemPath = ":/icons/icons/LeftRight.png";
            if(isLeft && isUp)
                itemPath = ":/icons/icons/UpLeft.png";
            if(isRight && isDown)
                itemPath = ":/icons/icons/DownRight.png";
            if(isRight && isUp)
                itemPath = ":/icons/icons/UpRight.png";
            if(isUp && isDown)
                itemPath = ":/icons/icons/UpDown.png";

            outputs[indexRow][indexCol].imagePath = itemPath;
        }
    }
    resultFile.close();
    return true;
}

bool Generator::isResultValid(QString cnfFileName, QString resultFileName, QStringList& newConstrain)
{
    QFile resultFile(resultFileName);
    if(!resultFile.open(QFile::ReadOnly)){
        qDebug() << "Open " + resultFileName + " error";
        return false;
    }

    QString prevResultLine;
    QTextStream resultStream(&resultFile);
    resultStream.readLine().trimmed(); //First line : SAT or UNSAT
    prevResultLine = resultStream.readLine(); //Second line: Result
    resultFile.close();
    QStringList variableValues = prevResultLine.trimmed().split(" ");

    SolutionItem tmpResult[MAX_INPUT_SIZE][MAX_INPUT_SIZE];
    for(int i = 1;  i <= m_rowcount; i++)
        for(int j = 1; j <= m_columncount; j++)
            tmpResult[i][j] = outputs[i][j];

    for(int i = 1; i <= m_rowcount; i++){
        for(int j = 1; j  <= m_columncount; j++){
            if(isNumber(inputs[i][j])){
                //                qDebug() << "## Remove number at " << i << " : " << j;
                removeResultPosition(i,j,tmpResult);
            }
        }
    }

    QList<int> wrongCellIndexes;
    int count = 0;
    for(int i = 1; i <= m_rowcount; i++){
        for(int j = 1; j  <= m_columncount; j++){
            if(tmpResult[i][j].isLeft || tmpResult[i][j].isRight || tmpResult[i][j].isUp || tmpResult[i][j].isDown){
                count ++;
                wrongCellIndexes.append((i-1)*(NUM_OF_DIRECTION + m_maxNum) * m_columncount + (j-1) * (NUM_OF_DIRECTION + m_maxNum));
            }
        }
    }

    //    for(int i = 1; i <= m_rowcount; i++){
    //        for(int j = 1; j  <= m_columncount; j++){
    //            if(isNumber(inputs[i][j])){
    //                qDebug() << "## Remove number at " << i << " : " << j;
    //                removeResultPosition(i,j,outputs);
    //            }
    //        }
    //    }

    //    QList<int> wrongCellIndexes;
    //    int count = 0;
    //    for(int i = 1; i <= m_rowcount; i++){
    //        for(int j = 1; j  <= m_columncount; j++){
    //            if(outputs[i][j].isLeft || outputs[i][j].isRight || outputs[i][j].isUp || outputs[i][j].isDown){
    //                count ++;
    //                wrongCellIndexes.append((i-1)*(NUM_OF_DIRECTION + m_maxNum) * m_columncount + (j-1) * (NUM_OF_DIRECTION + m_maxNum));
    //            }
    //        }
    //    }


    newConstrain.clear();
    foreach(int index, wrongCellIndexes){
        for(int i = 0; i < NUM_OF_DIRECTION; i++){
            newConstrain << variableValues[index + i] + " ";
        }
    }
    newConstrain << " 0";

    qDebug() << "### count " << count;
    qDebug() << "### PrevResults: " << prevResultLine;
    qDebug() << "### newContrain: " << newConstrain;
    if(count > 0)
        return false;
    return true;
}

void Generator::removeResultPosition(int i, int j, SolutionItem results[MAX_INPUT_SIZE][MAX_INPUT_SIZE])
{
    int i0, j0;
    do  {
        if(!( i >= 1 && i <= m_rowcount && j >= 1 && j <= m_columncount))
            break;
        //        qDebug() << "** Remove pattern at " << i << " : " << j;
//        qDebug() << "** Remove pattern";
        if(results[i][j].isLeft){
            qDebug() << "** ==> LEFT";
            i0 = DIR[LEFT][0];
            j0 = DIR[LEFT][1];
            results[i][j].isLeft = false;
            if(i+i0 >= 1 && i+i0 <= m_rowcount && j+j0 >= 1 && j+j0 <= m_columncount)
                results[i+i0][j+j0].isRight = false;
        }
        else if(results[i][j].isRight){
            qDebug() << "** ==> RIGHT";
            i0 = DIR[RIGHT][0];
            j0 = DIR[RIGHT][1];
            results[i][j].isRight = false;
            if(i+i0 >= 1 && i+i0 <= m_rowcount && j+j0 >= 1 && j+j0 <= m_columncount)
                results[i+i0][j+j0].isLeft = false;
        }
        else if(results[i][j].isUp){
            qDebug() << "** ==> UP";
            i0 = DIR[UP][0];
            j0 = DIR[UP][1];
            results[i][j].isUp = false;
            if(i+i0 >= 1 && i+i0 <= m_rowcount && j+j0 >= 1 && j+j0 <= m_columncount)
                results[i+i0][j+j0].isDown = false;
        }
        else if(results[i][j].isDown){
            qDebug() << "** ==> DOWN";
            i0 = DIR[DOWN][0];
            j0 = DIR[DOWN][1];
            results[i][j].isDown = false;
            if(i+i0 >= 1 && i+i0 <= m_rowcount && j+j0 >= 1 && j+j0 <= m_columncount)
                results[i+i0][j+j0].isUp = false;
        }
        i += i0;
        j += j0;
    }  while( !(i >= 1 && i <= m_rowcount && j >= 1 && j <= m_columncount) || !results[i][j].isNumber) ;
}

bool Generator::findSolution(QString inputFileName, QString cnfFileName, QString outputFileName){
    runMinisat(cnfFileName,outputFileName);
    QStringList newConstrain;
    bool parseStatus = false;
    bool resultStatus = false;
    while(true){
        parseStatus = parseResults(inputFileName,outputFileName);

        //    SolutionItem tmpResult[MAX_INPUT_SIZE][MAX_INPUT_SIZE];
        //    for(int i = 1;  i <= m_rowcount; i++)
        //        for(int j = 1; j <= m_columncount; j++)
        //            tmpResult[i][j] = outputs[i][j];

        resultStatus = isResultValid(cnfFileName,outputFileName,newConstrain);
        //        qDebug() << "@@@@@@@@@@@ resultStatus = " << resultStatus;

        //    for(int i = 1; i <= m_rowcount; i++){
        //        for(int j = 1; j  <= m_columncount; j++){
        //            if(! (outputs[i][j].isLeft || outputs[i][j].isRight || outputs[i][j].isUp || outputs[i][j].isDown) ){
        //                if(! (tmpResult[i][j].isLeft || tmpResult[i][j].isRight || tmpResult[i][j].isUp || tmpResult[i][j].isDown))
        //                    outputs[i][j].imagePath = ":/icons/icons/Blank.png";
        //                else
        //                    outputs[i][j].imagePath = ":/icons/icons/Blank2.png";
        //            }
        //        }
        //    }
        qDebug() << "Constrain: " << newConstrain;
        if(! (parseStatus && !resultStatus)){
            break;
        }
        addConstrain2Input(cnfFileName,newConstrain);
        runMinisat(cnfFileName,outputFileName);
    }
    //    emit sigFindSolutionFinished(parseStatus);
    //    return (parseStatus);
    emit sigFindSolutionFinished(parseStatus && resultStatus);
    return (parseStatus && resultStatus);
}

void Generator::runMinisat(QString inputCnf, QString output)
{
    QString program = qApp->applicationDirPath() + "/minisat";
    qDebug() << "program : " << program;
    QStringList arguments;
    arguments << inputCnf << output;
    m_process->start(program, arguments);
    m_process->waitForFinished(30000);
    //    connect(m_process, SIGNAL(finished(int)), this, SLOT(onMinisatFinished(int)));
    qDebug() << "start running minisat";
}

void Generator::onMinisatFinished(int exit_status)
{
    qDebug() << "on minisat finished! " << exit_status;
    emit sigFindSolutionFinished(exit_status);
    //    disconnect(m_process, SIGNAL(finished(int)), this, SLOT(onMinisatFinished(int)));
}
bool Generator::tryNext(QString cnfFileName, QString resultFileName)
{
    QFile resultFile(resultFileName);
    if(!resultFile.open(QFile::ReadOnly)){
        qDebug() << "Open " + resultFileName + " error";
        return false;
    }

    QString prevResultLine;
    QTextStream resultStream(&resultFile);
    if((prevResultLine = resultStream.readLine().trimmed()) != "SAT")
    {
        qDebug() << "No SAT";
        resultFile.close();
        return false;
    }
    prevResultLine = resultStream.readLine();

    QStringList variableValues = prevResultLine.trimmed().split(" ");
    addConstrain2Input(cnfFileName,variableValues);
    return true;
}

void Generator::addConstrain2Input(QString cnfFileName, QStringList constrains)
{
    QFile cnfFile(cnfFileName);
    if(!cnfFile.open(QFile::ReadOnly)){
        qDebug() << "Open Read for " + cnfFileName + " error";
        return;
    }

    QString newInputLine = "";
    foreach(QString value, constrains){
        newInputLine += QString::number(value.toInt() * (-1)) + " ";
    }

    QTextStream cnfInStream(&cnfFile);
    QStringList allCnfLines = cnfInStream.readAll().trimmed().split("\n");
    qDebug() << "allCnfLines: " << allCnfLines.size();
    cnfFile.close();

    QString firstLine = allCnfLines.at(0);

    QStringList cnfInValues = firstLine.trimmed().split(" ");
    if(cnfInValues.size() != 4){
        qDebug() << "Wrong cnf input format : size = " << cnfInValues.size();
        return;
    }

    qDebug() << "firstLine = " << firstLine << " allCnfLines size = " << allCnfLines.size();
    allCnfLines.removeFirst();
    allCnfLines.append(newInputLine);

    int numOfVariable = cnfInValues.at(2).toInt();
    int numOfClause = cnfInValues.at(3).toInt();
    numOfClause++;
    firstLine = "p cnf " + QString::number(numOfVariable) + " " + QString::number(numOfClause);

    if(!cnfFile.open(QFile::WriteOnly)){
        qDebug() << "Open Write for " + cnfFileName + " error";
        return;
    }
    QTextStream outCnfStream(&cnfFile);
    outCnfStream << firstLine << endl;
    for (int i = 0; i < allCnfLines.size(); i++){
        outCnfStream << allCnfLines.at(i);
        if(i < allCnfLines.size() - 1)
            outCnfStream << endl;
    }
    cnfFile.close();
}

STATUS Generator::parseInput(QString inputFileName)
{
    QFile inputFile(inputFileName);
    QList<int> numberList;
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        QString line = in.readLine();
        QStringList firstLine = line.trimmed().split( " ");
        m_rowcount = firstLine.at(0).toInt();
        m_columncount = firstLine.at(1).toInt();

        QStringList varInLine;
        int row = 1;
        int col = 1;

        int tmpNum;
        bool isNumber;
        while (!in.atEnd())
        {
            line = in.readLine();
            varInLine = line.split(" ");
            col = 1;
            for(int i = 1; i <=  varInLine.size(); i++){
                inputs[row][col] = varInLine.at(i-1);
                col++;
                tmpNum = (varInLine.at(i-1)).toInt(&isNumber);
                if(isNumber){
                    numberList.append(tmpNum);
                }
            }
            row++;
        }
        qDebug() << "rowCount = " << m_rowcount << " columnCount = " << m_columncount;
        qStableSort(numberList.begin(), numberList.end());
        if(numberList.size() % 2 != 0){
            qDebug() << "Invalid input";
            inputFile.close();
            return INVALID_INPUT;
        }
        else{
            for(int i = 0; i < numberList.size(); i += 2){
                if(numberList.at(i) != numberList.at(i+1)){
                    qDebug() << "Invalid input: not enough number pairs";
                    inputFile.close();
                    return NOT_ENOUGH_PAIR;
                }
            }
        }
        m_maxNum = numberList.last();
        qDebug() << "maxNum = " << m_maxNum;

        m_limit[RIGHT] = m_columncount;
        m_limit[DOWN] = m_rowcount;
        inputFile.close();
        return VALID_INPUT;
    }
    return INVALID_INPUT;
}

void Generator::generateCnfFile(QString inputFileName,QString outputFileName)
{
    qDebug() << "Start : " << QDateTime::currentDateTime();
    QList<QStringList> rules;
    int clauseCount = 0;
    int variableCount;

    QFile outFile(outputFileName);
    STATUS parseStatus = parseInput(inputFileName);
    if(parseStatus != VALID_INPUT){
        qDebug() << "ParseStatus: " << parseStatus;
        qDebug() << "End : " << QDateTime::currentDateTime();
        return;
    }
    m_prevInputCnfName = outputFileName;

    for(int i = 1; i <= m_rowcount; i++){
        for(int j = 1; j <= m_columncount; j++){

            if(isNoBlankCell){
                QStringList baseRule = atleast_one_direction(i,j);
                clauseCount += baseRule.size();
                rules.append(baseRule);
            }

            if(isNumber(inputs[i][j])){
                QStringList rule1 = connectToNumCell(i,j,inputs[i][j].toInt());
                QStringList rule2 = exact_one_direction(i,j);
                QStringList rule3 = connect_same_number(i,j);

                clauseCount += rule1.size() + rule2.size() + rule3.size();

                rules.append(rule1);
                rules.append(rule2);
                rules.append(rule3);
            }
            else{
                QStringList rule1 = maybe_two_direction(i,j);
                QStringList rule2 = connect_same_number(i,j);
                QStringList rule3 = limit_boundary(i,j);
                QStringList rule4 = maybeBlankCell(i,j);
                QStringList rule5 = maybe_one_number(i,j);

                clauseCount += rule1.size() + rule2.size() + rule3.size() + rule4.size() + rule5.size();

                rules.append(rule1);
                rules.append(rule2);
                rules.append(rule3);
                rules.append(rule4);
                rules.append(rule5);
            }
        }
    }
    if(outFile.open(QFile::WriteOnly)){
        QTextStream outStream( &outFile );
        variableCount = m_rowcount * m_columncount * (NUM_OF_DIRECTION + m_maxNum);
        QString firstLine = "p cnf " + QString::number(variableCount) + " " + QString::number(clauseCount);
        outStream << firstLine << endl;
        qDebug() << "Variables: " << variableCount << " Clauses: " << clauseCount;
        foreach(QStringList curList, rules){
            foreach(QString rule, curList){
                //                qDebug() << rule;
                //                outFile.write((rule + "\n").toStdString().c_str());
                outStream << rule << endl;
            }
        }
        outFile.close();
        qDebug() << "End : " << QDateTime::currentDateTime();
    }
}

bool Generator::isNumber(QString inputString)
{
    bool ok = false;
    inputString.toInt(&ok);
    return ok;
}

int Generator::toPosition(int i, int j, int value)
{
    int result = (i - 1) * (NUM_OF_DIRECTION + m_maxNum) * m_columncount + (j - 1) *  (NUM_OF_DIRECTION + m_maxNum) + value;
    return result;
}

QStringList Generator::connectToNumCell(int i, int j, int num)
{
    int result = toPosition(i,j, NUM_OF_DIRECTION + num);
    QStringList resultStringList;

    QString exactNumLine = "";
    exactNumLine += QString::number(result) + " 0";
    resultStringList << exactNumLine;
    for(int k = 1; k <= m_maxNum; k++){
        if(k != num){
            exactNumLine =  QString::number(-toPosition(i,j, NUM_OF_DIRECTION + k)) + " 0";
            resultStringList << exactNumLine;
        }
    }
    return resultStringList;
}

QStringList Generator::exact_one_direction(int i, int j)
{
    QStringList resulStringList;

    QString firstLine = "";
    firstLine += (j > 1) ? QString::number(toPosition(i,j,LEFT)) + " " : "";
    firstLine += (j < m_limit[RIGHT]) ? QString::number(toPosition(i,j,RIGHT))  + " ": "";
    firstLine += (i > 1) ? QString::number(toPosition(i,j,UP))  + " ": "";
    firstLine += (i < m_limit[DOWN]) ? QString::number(toPosition(i,j,DOWN))  + " ": "";
    firstLine += " 0";
    resulStringList << firstLine;

    initPermutation();
    permutationResult.clear();
    generatePermutation(NUM_OF_DIRECTION, 2, 1, 0);

    //    qDebug() << "exact one direction : " <<  i << " " << j;
    foreach(QSet<int> singleSet, permutationResult){
        QString tmpString = "";
        foreach(int value, singleSet){
            tmpString += QString::number(-toPosition(i,j,value)) +  " ";
        }
        tmpString += " 0";
        resulStringList << tmpString;
    }

    return resulStringList;
}

QStringList Generator::atleast_one_direction(int i, int j)
{
    QStringList resulStringList;

    QString firstLine = "";
    firstLine += (j > 1) ? QString::number(toPosition(i,j,LEFT)) + " " : "";
    firstLine += (j < m_limit[RIGHT]) ? QString::number(toPosition(i,j,RIGHT))  + " ": "";
    firstLine += (i > 1) ? QString::number(toPosition(i,j,UP))  + " ": "";
    firstLine += (i < m_limit[DOWN]) ? QString::number(toPosition(i,j,DOWN))  + " ": "";
    firstLine += " 0";
    resulStringList << firstLine;
    return resulStringList;
}

QStringList Generator::maybeBlankCell(int i, int j)
{
    QStringList resulStringList;
    QString firstLine = "";
    firstLine += QString::number(toPosition(i,j,LEFT)) + " " +
            QString::number(toPosition(i,j,RIGHT))  + " " +
            QString::number(toPosition(i,j,UP))  + " " +
            QString::number(toPosition(i,j,DOWN));
    for(int k = 1; k <= m_maxNum; k++){
        resulStringList.append(firstLine + " " + QString::number(-toPosition(i,j,NUM_OF_DIRECTION + k)) + " 0");
    }
    return resulStringList;
}

QStringList Generator::maybe_two_direction(int i, int j)
{
    QStringList resulStringList;

    for(int k = 1; k <= NUM_OF_DIRECTION; k++){
        QString firstClause = QString::number(-toPosition(i,j,k)) + " ";
        for(int q = 1; q <= NUM_OF_DIRECTION; q++){
            if(q != k){
                firstClause +=  QString::number(toPosition(i,j,q)) + " ";
            }
        }
        firstClause += " 0";
        resulStringList << firstClause;

        initPermutation();
        permutationResult.clear();
        generatePermutation(NUM_OF_DIRECTION, 3, 1, 0);
        foreach(QSet<int> singleSet, permutationResult){
            if(!singleSet.contains(k))
                continue;
            QString tmpString = "";
            foreach(int value, singleSet){
                tmpString += QString::number(-toPosition(i,j,value)) +  " ";
            }
            tmpString += " 0";
            resulStringList << tmpString;
        }
    }
    return resulStringList;
}

QStringList Generator::connect_same_number(int i, int j)
{
    QStringList resulStringList;

    int i0, j0;
    QString atleastOneDirection;
    for(int k = 1; k <= NUM_OF_DIRECTION; k++){
        i0 = DIR[k][0];
        j0 = DIR[k][1];

        atleastOneDirection = QString::number(-toPosition(i,j,k)) + " ";
        //Consider the compatability between adjacent patterns
        switch(k){
        case LEFT:
            atleastOneDirection += QString::number(toPosition(i+i0,j+j0,RIGHT)) + " ";
            break;
        case RIGHT:
            atleastOneDirection += QString::number(toPosition(i+i0,j+j0,LEFT)) + " ";
            break;
        case UP:
            atleastOneDirection += QString::number(toPosition(i+i0,j+j0,DOWN)) + " ";
            break;
        case DOWN:
            atleastOneDirection += QString::number(toPosition(i+i0,j+j0,UP)) + " ";
            break;
        }
        atleastOneDirection += " 0";

        if( (k == RIGHT && (j + j0) <= m_limit[k]) || ( k == LEFT && (j + j0) >= m_limit[k] ) ) {
            resulStringList << atleastOneDirection;
            for(int q = NUM_OF_DIRECTION + 1; q <= NUM_OF_DIRECTION + m_maxNum; q++ ){
                QString tmpString = "";
                tmpString = QString::number(-toPosition(i,j,k)) + " ";
                tmpString += QString::number(-toPosition(i,j,q)) + " ";
                tmpString += QString::number(toPosition(i,j + j0,q)) + " ";
                tmpString += " 0";
                resulStringList << tmpString;

                tmpString = QString::number(-toPosition(i,j,k)) + " ";
                tmpString += QString::number(toPosition(i,j,q)) + " ";
                tmpString += QString::number(-toPosition(i,j + j0,q)) + " ";
                tmpString += " 0";
                resulStringList << tmpString;
            }
        }
        else  if( (k == DOWN && i + i0 <= m_limit[k]) || ( k == UP && i + i0 >= m_limit[k] ))  {
            resulStringList << atleastOneDirection;
            for(int q = NUM_OF_DIRECTION + 1; q <= NUM_OF_DIRECTION + m_maxNum; q++ ){
                QString  tmpString = QString::number(-toPosition(i,j,k)) + " ";
                tmpString += QString::number(-toPosition(i,j,q)) + " ";
                tmpString += QString::number(toPosition(i+i0,j,q)) + " ";
                tmpString += " 0";
                resulStringList << tmpString;

                tmpString = QString::number(-toPosition(i,j,k)) + " ";
                tmpString += QString::number(toPosition(i,j,q)) + " ";
                tmpString += QString::number(-toPosition(i+i0,j,q)) + " ";
                tmpString += " 0";
                resulStringList << tmpString;
            }
        }
    }
    return resulStringList;
}

QStringList Generator::maybe_one_number(int i, int j)
{
    QStringList resulStringList;

    QString atleastNum = "";
    for(int k = 1; k <= m_maxNum ; k++){
        atleastNum += QString::number(toPosition(i,j,NUM_OF_DIRECTION + k)) + " ";
    }

    QString curRule = "";
    for(int q = 1; q <= NUM_OF_DIRECTION; q++){
        curRule = QString::number(-toPosition(i,j,q));
        resulStringList << curRule + " " + atleastNum + " 0";
    }
    return resulStringList;
}

QStringList Generator::limit_boundary(int i, int j)
{
    QStringList resultStringList;

    if(j <= 1)
        resultStringList << QString::number(-toPosition(i,j,LEFT)) + " 0";
    if(j >= m_limit[RIGHT])
        resultStringList << QString::number(-toPosition(i,j,RIGHT)) + " 0";
    if(i <= 1)
        resultStringList << QString::number(-toPosition(i,j,UP)) + " 0";
    if(i >= m_limit[DOWN])
        resultStringList << QString::number(-toPosition(i,j,DOWN)) + " 0";

    return resultStringList;
}

void Generator::generatePermutation(int num, int base, int step, int prev)
{
    if(step >= base + 1){
        QSet<int> tmpSet;
        for(int i = 1; i <= num; i++){
            if(m_mark[i]){
                tmpSet.insert(i);
            }
        }
        permutationResult.append(tmpSet);
        return;
    }

    for(int i = 1; i <= num; i++){
        if(m_mark[i] == false && i > prev){
            m_mark[i] = true;
            generatePermutation(num,base,step+1,i);
            m_mark[i] = false;
        }
    }
}

int Generator::maxNum() const
{
    return m_maxNum;
}

void Generator::setMaxNum(int maxNum)
{
    m_maxNum = maxNum;
}

void Generator::initPermutation()
{
    for(int i = 0; i < MAX_PERMUTATION_SIZE; i++){
        m_mark[i] = false;
    }
}
QList< QSet<int> > Generator::getPermutationResult() const
{
    return permutationResult;
}


