#-------------------------------------------------
#
# Project created by QtCreator 2015-10-14T23:32:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CNFGenerator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    generator.cpp \
    numberlinkdisplay.cpp

HEADERS  += mainwindow.h \
    generator.h \
    numberlinkdisplay.h

FORMS    += mainwindow.ui

RESOURCES += \
    CNFResources.qrc
